#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 23:03:38 2019

@author: Marcin Szarejko
"""

# images annotations
train_dataset_annot = 'dataset/wider_face_split/wider_face_train_bbx_gt.txt'
val_dataset_annot = 'dataset/wider_face_split/wider_face_val_bbx_gt.txt'

# train and validation datasets
train_dataset = 'dataset/wider_train/'
val_dataset = 'dataset/wider_val/'

'''Define yolo input image size and output grid cells.
Lower resolution decreases accuracy yet is faster and higher resolution increases
accuracy decreasing speed. Try to keep 32 pixel wide grid cell.'''
yolo_input_size = (608, 608, 3)
yolo_output_size = (19, 19, 5)
grid_cell = 32

'''Define network parameters.
Yolo model uses (3, 3) and (1, 1) kernel convolution sizes in turns after each other
to reduce the dimensionality of filter space and draw out most important features.''' 
parameters = {
              'filters': [16, 32, [48, 32, 48], [64, 48, 64],
                         [80, 64, 80, 64, 80], [96, 80, 96, 80, 96]],
              'kernel_size': [(3, 3), (3, 3), [(3, 3), (1, 1), (3, 3)], [(3, 3), (1, 1), (3, 3)],
                         [(3, 3), (1, 1), (3, 3), (1, 1), (3, 3)], [(3, 3), (1, 1), (3, 3), (1, 1), (3, 3)]]}

'''Define model's hyperparameters.
We want our network to focus more on adjsting the bounding boxes around faces, 
so we multiply the loss from classification in grid cell with lambda_object,
and multiply loss from bounding box with lambda_coordinates which is of greater value.
However, we only scale the clasification error with grids where no face is present.
This ensure that grid cells with no faces don't overpower the rest of the grid cells.'''
learning_rate = 0.001
weight_deay = 0.0005
momentum = 0.9
lambda_object = .5
lambda_coordinates = 5
batch_size = 8
num_epochs = 200
