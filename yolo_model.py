#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 16:46:23 2019

@author: Marcin Szarejko

@credits: You Only Look Once: Unified, Real-Time Object Detection
YOLO9000: Better, Faster, Stronger 
"""

import hyperparameters as hp
import dataset_preprocessing as dp
import tensorflow as tf
import numpy as np


class yolo_v2():
    def __init__(self):
        '''Create default pipeline for yolo training process.'''
        tf.reset_default_graph()
        self.graph = tf.get_default_graph()
        with self.graph.as_default():
            self.X, self.Y = self.create_placeholders(hp.yolo_input_size, hp.yolo_output_size)
            self.grid_predictions = self.forward_propagation(self.X)
            self.loss = self.loss_function(self.grid_predictions, self.Y)
        
    def create_placeholders(self, yolo_input_size, yolo_output_size):
        '''Create placeholders to feed in the data to a neural network.'''
        X = tf.placeholder(dtype = tf.float32, shape = (None, *yolo_input_size))
        Y = tf.placeholder(dtype = tf.float32, shape = (None, *yolo_output_size))
        return X, Y
    
    def conv2d(self, X, num_filters, kernel_size, trainable):
        '''Wrapper for a single pass through a convolution function with activation function afterwards.
        Batch normalization layers to speed up the training process as well as to help stabilize it in the early stage.'''
        X = tf.keras.layers.Conv2D(filters = num_filters, kernel_size = kernel_size, padding = 'same', trainable = trainable).apply(X)
        X = tf.nn.leaky_relu(X, alpha = 0.1)
        X = tf.keras.layers.BatchNormalization().apply(X)
        return X
        
    def maxpool(self, X):
        '''Wrapper for max pooling operation, pool_size and strides set for a whole network.'''
        return tf.keras.layers.MaxPool2D(pool_size = (2, 2), strides = (2, 2)).apply(X)
    
    def conv_block(self, X, filters, kernel_sizes, trainable):
        '''Perform forward propagation on a convolutional block with predefined filter and kernel sizes.'''
        for num_filters, kernel_size in zip(filters, kernel_sizes):
            X = self.conv2d(X, num_filters, kernel_size, trainable)
        return X
        
    def forward_propagation(self, X, trainable = True):
        '''Define the neural network architecture using previously defined wrappers to minimize the amount of code.
        Returns convolved image/batch of images after a forward pass through the convnet.'''
        X = self.conv2d(X, hp.parameters['filters'][0], hp.parameters['kernel_size'][0], trainable)
        X = self.maxpool(X)
        X = self.conv2d(X, hp.parameters['filters'][1], hp.parameters['kernel_size'][1], trainable)
        X = self.maxpool(X)
        X = self.conv_block(X, hp.parameters['filters'][2], hp.parameters['kernel_size'][2], trainable)
        X = self.maxpool(X)
        X = self.conv_block(X, hp.parameters['filters'][3], hp.parameters['kernel_size'][3], trainable)
        X = self.maxpool(X)
        X = self.conv_block(X, hp.parameters['filters'][4], hp.parameters['kernel_size'][4], trainable)
        X = self.maxpool(X)
        X = self.conv_block(X, hp.parameters['filters'][5], hp.parameters['kernel_size'][5], trainable)
        return X
    
    def loss_function(self, prediction, Y_true):
        '''Construct custom loss function for training the yolo model.
        Full reasoning behind the choice of this loss function is available in yolo paper.'''
        face_tensor = Y_true[:,0,:,:]
        # compute classification loss for cells containing faces
        class_loss = tf.reduce_sum(tf.multiply(face_tensor, tf.squared_difference(face_tensor, prediction[:,0,:,:])))
        # compute classification loss for cells without any face
        no_face_loss = tf.reduce_sum(tf.multiply(1 - face_tensor, tf.squared_difference(face_tensor, prediction[:,0,:,:])))
        # compute face coordinates prediction loss
        coordinate_loss = tf.reduce_sum(tf.multiply(face_tensor, tf.squared_difference(Y_true[:,1:3,:,:], prediction[:,1:3,:,:])))
        # face width and height prediction loss
        '''Small deviations of predicted bounding box matter more when the face is small, not so much when the face is large,
        so we can adress this by taking a square root of the values before applying squared error.'''
        boundaries_loss = tf.reduce_sum(tf.multiply(face_tensor, tf.squared_difference(tf.sqrt(Y_true[:,3:,:,:]), tf.sqrt(prediction[:,3:,:,:]))))
        
        '''Now we want our model to be more focused on predicting correct bounding boxes so we scale the loss from those predictions
        by a lambda_coordinates constant and scale classification prediction when the face is not present in a grid cell by lambda_face.'''
        lambda_coordinates = tf.constant(hp.lambda_coordinates)
        lambda_face = tf.constant(hp.lambda_face) 
        loss = tf.add_n([tf.scalar_mul(lambda_coordinates, coordinate_loss),
                         boundaries_loss,
                         tf.scalar_mul(lambda_face, no_face_loss),
                         class_loss]) 
        return loss
    
    def train(self, X):
        '''Initializes the weights of the yolo model.
        Performs the training process, saving the learned weights in a .hdf5 file.
        Returns list of losses for visualization purposes.'''
        init = tf.global_variables_initializer()
        with tf.Session() as sess:
            sess.run(init)
            preds = sess.run(self.grid_predictions, feed_dict = {self.X: X})
        return preds