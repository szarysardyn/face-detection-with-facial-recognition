#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 12:31:03 2019

@author: Marcin Szarejko
"""

import cv2
import os
import numpy as np
import hyperparameters as hp
from hyperparameters import yolo_input_size, yolo_output_size
from re import findall


# specify where you want to save processed images
file_path = os.path.dirname(__file__)
train_path = os.path.join(file_path, 'dataset/train_set')
val_path = os.path.join(file_path, 'dataset/val_set')

def convert_coordinates(x, y, w, h):
    # convert top left coordinatess to mid and corners
    mid = (x + int(w/2), y + int(h/2))
    x1, x2 = x, x + w
    y1, y2 = y, y + h
    return mid, (x1, y1, x2, y2)

def intersection_over_union(self, box1, box2):
    (box1_x1, box1_y1, box1_x2, box1_y2) = box1
    (box2_x1, box2_y1, box2_x2, box2_y2) = box2
    
    # calculate intersection coordinates
    xi1 = max(box1_x1, box2_x1)
    xi2 = min(box1_x2, box2_x2)
    yi1 = max(box1_y1, box2_y1)
    yi2 = min(box1_y2, box2_y2)
    
    # calculate intersection area
    inner_width = max(0, xi2 - xi1)
    inner_height = max(0, yi2 - yi1)
    inner_area = inner_width * inner_height
    
    # calculate union
    box1_area = (box1_x2 - box1_x1) * (box1_y2 - box1_y1)
    box2_area = (box2_x2 - box2_x1) * (box2_y2 - box2_y1)
    union = box1_area + box2_area - inner_area
    
    iou = inner_area / union
    return iou

def get_annotations(dataset_dir):
    img_annotations = []
    with open(os.path.join(file_path, dataset_dir), 'r') as file:
        lines = file.readlines()
        for i in range(len(lines)):
            # find image names, and accept only those with less than 10 faces
            if findall('.jpg$', lines[i]) and int(lines[i+1]) <= 10:
                img_annot = [line.rstrip() for line in lines[i : i+2 + int(lines[i+1])]]
                img_annot[2:] = [list(map(int, annot.split()[:4])) for annot in img_annot[2:]]
                img_annotations.append(img_annot)
    return img_annotations

def show_images():
    train_annot = get_annotations(hp.train_dataset_annot)
    train_dataset_dir = os.path.join(file_path, hp.train_dataset)
    
    for img_annot in train_annot:
        img = cv2.imread(os.path.join(train_dataset_dir, img_annot[0]))
        img, boxes = resize_image(img, img_annot[2:])
        for box in boxes:
            _, (x1, y1, x2, y2) = convert_coordinates(*box)
            cv2.rectangle(img, (x1, y1), (x2, y2), (0, 255, 255), 1)
        cv2.imshow('image', img)
        cv2.waitKey()

def resize_image(img, boxes):
    # calculate scale to preserve aspect ratio
    height, width = img.shape[:2]
    main_dim = max(height, width)
    scale = yolo_input_size[0] / main_dim
    
    # rescale width and height
    target_height = int(height*scale) if int(height*scale) % 2 == 0 else int(height*scale) + 1 
    target_width = int(width*scale) if int(width*scale) % 2 == 0 else int(width*scale) + 1
    img = cv2.resize(img, (target_width, target_height))
    
    # pad the image to fit model's input size
    side_pad = int((yolo_input_size[0] - target_width)/2)
    up_pad = int((yolo_input_size[0] - target_height)/2)
    img = cv2.copyMakeBorder(img, up_pad, up_pad, side_pad, side_pad, cv2.BORDER_CONSTANT)
    
    # change boxes after resizing and padding image
    scaled_boxes = [[int(coordinate * scale) + side_pad if coordinate == box[0] else int(coordinate * scale) + up_pad
                     if coordinate == box[1] else int(coordinate * scale) for coordinate in box] for box in boxes]
    
    return img, scaled_boxes

def convert_to_yolo(boxes):
    # initialize yolo output tensor
    boxes_tensor = np.zeros(yolo_output_size)
    # insert face annotation for each cell containing face
    for box in boxes:
        (x, y), _ = convert_coordinates(*box)
        x_idx = int(x/hp.grid_cell) if int(x/hp.grid_cell) <= yolo_output_size[0] else yolo_output_size[0] - 1
        y_idx = int(y/hp.grid_cell) if int(y/hp.grid_cell) <= yolo_output_size[0] else yolo_output_size[0] - 1
        boxes_tensor[x_idx][y_idx][0] = 1
        boxes_tensor[x_idx][y_idx][1:] = box
    return boxes_tensor

def preprocess_images():
    # get all image names and face annotations
    train_annot = get_annotations(hp.train_dataset_annot)
    np.random.shuffle(train_annot)
    train_dataset_dir = os.path.join(file_path, hp.train_dataset)
    val_annot = get_annotations(hp.val_dataset_annot)
    np.random.shuffle(val_annot)
    val_dataset_dir = os.path.join(file_path, hp.val_dataset)

    # preprocess the images into yolo format and save image names along with annotation in txt file
    with open('dataset/train_set/0_0train_imgs.txt', 'w+') as train_imgs:
        for img_annot in train_annot:
            img = cv2.imread(os.path.join(train_dataset_dir, img_annot[0]))
            img, boxes = resize_image(img, img_annot[2:])
            cv2.imwrite(os.path.join(train_path, img_annot[0].split('/')[1]), img)
            img_annot[2:] = [' '.join([str(annot) for annot in box]) for box in boxes]
            img_annot.append('\n')
            train_imgs.write(' | '.join(img_annot))
    
    # do the same as above with validation data
    with open('dataset/val_set/0_0val_imgs.txt', 'w+') as val_imgs:
        for img_annot in val_annot:
            img = cv2.imread(os.path.join(val_dataset_dir, img_annot[0]))
            img, boxes = resize_image(img, img_annot[2:])
            cv2.imwrite(os.path.join(val_path, img_annot[0].split('/')[1]), img)
            img_annot[2:] = [' '.join([str(annot) for annot in box]) for box in boxes]
            img_annot.append('\n')
            val_imgs.write(' | '.join(img_annot))